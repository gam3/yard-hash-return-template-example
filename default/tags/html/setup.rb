
STDERR.puts 'ASDFASDf'
# frozen_string_literal: true
def init
  tags = Tags::Library.visible_tags - [:abstract, :deprecated, :note, :todo]
  create_tag_methods(tags - [ :myreturn ])
  sections :index, tags.map {|t| t.to_s.tr('.', '_').to_sym }
  STDERR.puts sections;
  sections.any(:overload).push(T('docstring'))
end
